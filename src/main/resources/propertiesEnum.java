package main.resources;

public enum propertiesEnum {
  DB_URL("jdbc:sqlserver://localhost:1433;databaseName=dbsch1;trustServerCertificate=true;"),
  DB_USERNAME("SA"),
  DB_PASSWORD("9)1*8@2U");

  private final String value;

  propertiesEnum(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }
}
