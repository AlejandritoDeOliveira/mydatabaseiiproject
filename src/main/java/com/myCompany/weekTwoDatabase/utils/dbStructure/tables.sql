CREATE TABLE grade (
    id int IDENTITY(1,1) NOT NULL,
    value int  NOT NULL,
    letter varchar(1)  NOT NULL,
    subjects_per_student_student_id int  NOT NULL,
    subjects_per_student_subject_id int  NOT NULL,
    CONSTRAINT grade_pk PRIMARY KEY (id)
);
CREATE TABLE professor (
    id int IDENTITY(1,1) NOT NULL,
    name varchar(50)  NOT NULL,
    phone varchar(7)  NULL,
    subject_id int  NOT NULL,
    CONSTRAINT professor_pk PRIMARY KEY (id)
);
CREATE TABLE student (
    id int IDENTITY(1,1) NOT NULL,
    name varchar(50)  NOT NULL,
    phone varchar(7)  NULL,
    CONSTRAINT student_pk PRIMARY KEY (id)
);
CREATE TABLE subject (
    id int IDENTITY(1,1) NOT NULL,
    name varchar(50)  NOT NULL,
    CONSTRAINT subject_pk PRIMARY KEY (id)
);
CREATE TABLE subjects_per_student (
    student_id int  NOT NULL,
    subject_id int  NOT NULL,
    CONSTRAINT subjects_per_student_pk PRIMARY KEY (student_id,subject_id)
);
ALTER TABLE grade ADD CONSTRAINT grade_subjects_per_student
    FOREIGN KEY (subjects_per_student_student_id, subjects_per_student_subject_id)
    REFERENCES subjects_per_student (student_id, subject_id)
;

ALTER TABLE professor ADD CONSTRAINT professor_subject
    FOREIGN KEY (subject_id)
    REFERENCES subject (id)
;
ALTER TABLE subjects_per_student ADD CONSTRAINT subjects_per_student_student
    FOREIGN KEY (student_id)
    REFERENCES student (id)
;
ALTER TABLE subjects_per_student ADD CONSTRAINT subjects_per_student_subject
    FOREIGN KEY (subject_id)
    REFERENCES subject (id)
;
