CREATE PROCEDURE set_grade
    @value INT,
    @student_id INT,
    @subject_id INT
AS
BEGIN
    DECLARE @letter VARCHAR(2)

    IF NOT EXISTS (
        SELECT 1
        FROM subjects_per_student
        WHERE subject_id = @subject_id
          AND student_id = @student_id
    )
    BEGIN
        THROW 51000, 'The subject is not assigned to the student.', 1;
        RETURN;
    END

    IF @value >= 90
        SET @letter = 'A'
    ELSE IF @value >= 80
        SET @letter = 'B'
    ELSE IF @value >= 70
        SET @letter = 'C'
    ELSE IF @value >= 60
        SET @letter = 'D'
    ELSE
        SET @letter = 'F'

    INSERT INTO grade (value, letter, subjects_per_student_student_id, subjects_per_student_subject_id)
    VALUES (@value, @letter, @student_id, @subject_id)
END;

CREATE PROCEDURE storage_grades
AS
BEGIN
    IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'grades_history')
    BEGIN
        CREATE TABLE grades_history (
            id INT IDENTITY(1,1) PRIMARY KEY, -- Useing IDENTITY to autoincrement
            value INT,
            letter VARCHAR(1),
            subjects_per_student_subject_id INT,
            subjects_per_student_student_id INT,
            year INT
        );
    END;

    DECLARE @CurrentYear INT
    SET @CurrentYear = YEAR(GETDATE())

    INSERT INTO grades_history (value, letter, subjects_per_student_subject_id, subjects_per_student_student_id, year)
    SELECT value, letter, subjects_per_student_subject_id, subjects_per_student_student_id, @CurrentYear
    FROM grade;


    DELETE FROM grade;

    PRINT 'Data migration completed.';
END;


