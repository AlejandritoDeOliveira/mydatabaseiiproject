CREATE TRIGGER validate_grade_letter
ON grade
INSTEAD OF INSERT
AS
BEGIN

    SET IDENTITY_INSERT grade ON;

    IF NOT EXISTS (
        SELECT 1
        FROM inserted
        WHERE letter IN ('A', 'B', 'C', 'F')
    )
    BEGIN
        THROW 51000, 'Invalid letter.', 1;
        ROLLBACK TRANSACTION;
    END
    ELSE
    BEGIN
        INSERT INTO grade (id, value, letter, subjects_per_student_student_id, subjects_per_student_subject_id)
        SELECT id, value, letter, subjects_per_student_student_id, subjects_per_student_subject_id
        FROM inserted;
    END

    SET IDENTITY_INSERT grade OFF;
END;
