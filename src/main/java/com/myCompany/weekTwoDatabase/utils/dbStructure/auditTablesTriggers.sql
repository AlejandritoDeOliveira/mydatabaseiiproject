CREATE TRIGGER professor_audit_trigger
ON professor
AFTER INSERT, UPDATE, DELETE
AS
BEGIN
    DECLARE @action NVARCHAR(10);
    SET @action = CASE
        WHEN EXISTS (SELECT * FROM inserted) AND EXISTS (SELECT * FROM deleted) THEN 'UPDATE'
        WHEN EXISTS (SELECT * FROM inserted) THEN 'INSERT'
        ELSE 'DELETE'
    END;

    IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'professor_audit')
    BEGIN
        CREATE TABLE professor_audit (
            action NVARCHAR(10),
            timestamp DATETIME,
            id INT,
            name NVARCHAR(50),
            phone VARCHAR(7)
        );
    END;

    INSERT INTO professor_audit (action, timestamp, id, name, phone)
    SELECT
        @action,
        GETDATE(),
        COALESCE(i.id, d.id),
        COALESCE(i.name, d.name),
        COALESCE(i.phone, d.phone)
    FROM inserted i
    FULL OUTER JOIN deleted d ON i.id = d.id;
END;

CREATE TRIGGER subject_audit_trigger
ON subject
AFTER INSERT, UPDATE, DELETE
AS
BEGIN
    DECLARE @action NVARCHAR(10);
    SET @action = CASE
        WHEN EXISTS (SELECT * FROM inserted) AND EXISTS (SELECT * FROM deleted) THEN 'UPDATE'
        WHEN EXISTS (SELECT * FROM inserted) THEN 'INSERT'
        ELSE 'DELETE'
    END;

    IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'subject_audit')
    BEGIN
        CREATE TABLE subject_audit (
            action NVARCHAR(10),
            timestamp DATETIME,
            id INT,
            name NVARCHAR(50)
        );
    END;

    INSERT INTO subject_audit (action, timestamp, id, name)
    SELECT
        @action,
        GETDATE(),
        COALESCE(i.id, d.id),
        COALESCE(i.name, d.name)
    FROM inserted i
    FULL OUTER JOIN deleted d ON i.id = d.id;
END;

CREATE TRIGGER subjects_per_student_audit_trigger
ON subjects_per_student
AFTER INSERT, UPDATE, DELETE
AS
BEGIN
    DECLARE @action NVARCHAR(10);
    SET @action = CASE
        WHEN EXISTS (SELECT * FROM inserted) AND EXISTS (SELECT * FROM deleted) THEN 'UPDATE'
        WHEN EXISTS (SELECT * FROM inserted) THEN 'INSERT'
        ELSE 'DELETE'
    END;

    IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'subjects_per_student_audit')
    BEGIN
        CREATE TABLE subjects_per_student_audit (
            action NVARCHAR(10),
            timestamp DATETIME,
            student_id INT,
            subject_id INT
        );
    END;

    INSERT INTO subjects_per_student_audit (action, timestamp, student_id, subject_id)
    SELECT
        @action,
        GETDATE(),
        COALESCE(i.student_id, d.student_id),
        COALESCE(i.subject_id, d.subject_id)
    FROM inserted i
    FULL OUTER JOIN deleted d ON i.student_id = d.student_id AND i.subject_id = d.subject_id;
END;

CREATE TRIGGER grade_audit_trigger
ON grade
AFTER INSERT, UPDATE, DELETE
AS
BEGIN
    DECLARE @action NVARCHAR(10);
    SET @action = CASE
        WHEN EXISTS (SELECT * FROM inserted) AND EXISTS (SELECT * FROM deleted) THEN 'UPDATE'
        WHEN EXISTS (SELECT * FROM inserted) THEN 'INSERT'
        ELSE 'DELETE'
    END;

    IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'grade_audit')
    BEGIN
        CREATE TABLE grade_audit (
            action NVARCHAR(10),
            timestamp DATETIME,
            id INT,
            value INT,
            letter VARCHAR(1),
            subjects_per_student_student_id INT,
            subjects_per_student_subject_id INT
        );
    END;

    INSERT INTO grade_audit (action, timestamp, id, value, letter, subjects_per_student_student_id, subjects_per_student_subject_id)
    SELECT
        @action,
        GETDATE(),
        COALESCE(i.id, d.id),
        COALESCE(i.value, d.value),
        COALESCE(i.letter, d.letter),
        COALESCE(i.subjects_per_student_student_id, d.subjects_per_student_student_id),
        COALESCE(i.subjects_per_student_subject_id, d.subjects_per_student_subject_id)
    FROM inserted i
    FULL OUTER JOIN deleted d ON i.id = d.id;
END;

CREATE TRIGGER student_audit_trigger
ON student
AFTER INSERT, UPDATE, DELETE
AS
BEGIN
    DECLARE @action NVARCHAR(10);
    SET @action = CASE
        WHEN EXISTS (SELECT * FROM inserted) AND EXISTS (SELECT * FROM deleted) THEN 'UPDATE'
        WHEN EXISTS (SELECT * FROM inserted) THEN 'INSERT'
        ELSE 'DELETE'
    END;

    IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'student_audit')
    BEGIN
    CREATE TABLE student_audit (
        action NVARCHAR(10),
        timestamp DATETIME,
        id INT,
        name NVARCHAR(50),
        phone VARCHAR(7)
    );
    END;

    INSERT INTO student_audit (action, timestamp, id, name, phone)
    SELECT
        @action,
        GETDATE(),
        COALESCE(i.id, d.id),
        COALESCE(i.name, d.name),
        COALESCE(i.phone, d.phone)
    FROM inserted i
    FULL OUTER JOIN deleted d ON i.id = d.id;
END;
