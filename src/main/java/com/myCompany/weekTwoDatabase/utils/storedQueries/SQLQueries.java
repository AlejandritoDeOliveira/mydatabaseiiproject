package main.java.com.myCompany.weekTwoDatabase.utils.storedQueries;

public enum SQLQueries {

  // Student stored queries
  INSERT_STUDENT("INSERT INTO student (name, phone) VALUES (?, ?);"),
  GET_STUDENT_BY_ID("SELECT * FROM student WHERE id = ?;"),
  GET_ALL_STUDENTS("SELECT * FROM student;"),
  UPDATE_STUDENT("UPDATE student SET name = ?, phone = ? WHERE id = ?;"),
  DELETE_STUDENT("DELETE FROM student WHERE id = ?;");

  // other tables stored queries


  private final String queryString;

  SQLQueries(String queryString) {
    this.queryString = queryString;
  }

  public String getQueryString() {
    return queryString;
  }
}