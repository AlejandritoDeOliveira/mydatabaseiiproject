package main.java.com.myCompany.weekTwoDatabase;

import main.java.com.myCompany.weekTwoDatabase.collections.StudentCRUD;
import main.java.com.myCompany.weekTwoDatabase.model.Student;

public class Main {
    public static void main(String[] args) {
        StudentCRUD studentOps = new StudentCRUD();

        // Let us add some students

/*        studentOps.addStudent(
            new Student("Alejandro De Oliveira", "6830272")
        );

        studentOps.addStudent(
            new Student("Emily Ana Anagua", "1234567")
        );

        studentOps.addStudent(
            new Student("Pepito Perez", "6457485")
        );

        studentOps.addStudent(
            new Student("Donald Trump", "991-234")
        );*/

        // Let us delete the extra ones we added by mistake

/*        studentOps.delete(5);
        studentOps.delete(6);
        studentOps.delete(7);*/

        // Let us display the students in the console

/*        for (Object student : studentOps.getAll()) {
            System.out.println(
                student.toString()
            );
        }*/

        // Let us modify Emily's phone
/*        Student Emily = new Student("Emily Ana Anagua", "9234252");
        Emily.setId(2);

        studentOps.update(Emily);*/

        // Now updated, let's see Emily information
/*        System.out.println(
            studentOps.getById(2).toString()
        );*/


    }
}





