package main.java.com.myCompany.weekTwoDatabase.model;

public class Person {

 protected Integer id;
 protected String name;
 protected String phone;

 public Person(String name, String phone) {
  this.name = name;
  this.phone = phone;
 }

 public Integer getId() {
  return id;
 }

 public void setId(Integer id) {
  this.id = id;
 }

 public String getName() {
  return name;
 }

 public void setName(String name) {
  this.name = name;
 }

 public String getPhone() {
  return phone;
 }

 public void setPhone(String phone) {
  this.phone = phone;
 }

 public String toString() {
   return id + " Name: " + name + ". Phone: " + phone;
 }
}
