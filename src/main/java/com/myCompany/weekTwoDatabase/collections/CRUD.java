package main.java.com.myCompany.weekTwoDatabase.collections;

import java.util.List;

public interface CRUD {

  // Create
  boolean addStudent(Object object);

  // Read
  Object getById(int id);
  List<Object> getAll();

  // Update operation
  boolean update(Object object);

  // Delete operation
  boolean delete(int id);

}
