package main.java.com.myCompany.weekTwoDatabase.collections;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import main.resources.propertiesEnum;

public class DbConnector {

  private final String dbUrl;
  private final String dbUsername;
  private final String dbPassword;

  public DbConnector(String dbUrl, String dbUsername, String dbPassword) {
    this.dbUrl = dbUrl;
    this.dbUsername = dbUsername;
    this.dbPassword = dbPassword;
  }

  public static DbConnector loadFromPropertiesFile() throws IOException {
    Properties properties = new Properties();

    String dbUrl = propertiesEnum.DB_URL.getValue();
    String dbUsername = propertiesEnum.DB_USERNAME.getValue();
    String dbPassword = propertiesEnum.DB_PASSWORD.getValue();

    return new DbConnector(dbUrl, dbUsername, dbPassword);
  }

  public Connection getConnection() {
    try {
      return DriverManager.getConnection(dbUrl, dbUsername, dbPassword);
    } catch (SQLException e) {
      throw new RuntimeException("Failed to connect to the database.", e);
    }
  }
}
