package main.java.com.myCompany.weekTwoDatabase.collections;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import main.java.com.myCompany.weekTwoDatabase.model.Student;
import main.java.com.myCompany.weekTwoDatabase.utils.storedQueries.SQLQueries;

public class StudentCRUD implements CRUD {

  @Override
  public boolean addStudent(Object object) {
    try {
      DbConnector dbConnector = DbConnector.loadFromPropertiesFile();

      try (Connection connection = dbConnector.getConnection()) {

        Student student = (Student) object;

        PreparedStatement preparedStatement = connection.prepareStatement(SQLQueries.INSERT_STUDENT.getQueryString());
        preparedStatement.setString(1, student.getName());
        preparedStatement.setString(2, student.getPhone());

        int rowsInserted = preparedStatement.executeUpdate();

        return rowsInserted > 0;
      }

    } catch (IOException e) {
      e.printStackTrace();

    } catch (Exception e) {
      e.printStackTrace();
    }

    return false;
  }


  @Override
  public Object getById(int id) {
    try {
      DbConnector dbConnector = DbConnector.loadFromPropertiesFile();

      try (Connection connection = dbConnector.getConnection()) {

        PreparedStatement preparedStatement = connection.prepareStatement(SQLQueries.GET_STUDENT_BY_ID.getQueryString());
        preparedStatement.setInt(1, id);

        ResultSet resultSet = preparedStatement.executeQuery();

        if (resultSet.next()) {
          Student student = new Student(
              resultSet.getString("name"),
              resultSet.getString("phone"));
          student.setId(resultSet.getInt("id"));
          return student;
        }
      }

    } catch (IOException e) {
      e.printStackTrace();

    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }


  @Override
  public List<Object> getAll() {
    List<Object> students = new ArrayList<>();

    try {
      DbConnector dbConnector = DbConnector.loadFromPropertiesFile();

      try (Connection connection = dbConnector.getConnection()) {

        PreparedStatement preparedStatement = connection.prepareStatement(SQLQueries.GET_ALL_STUDENTS.getQueryString());

        ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) {
          Student student = new Student(
              resultSet.getString("name"),
              resultSet.getString("phone"));
          student.setId(resultSet.getInt("id"));
          students.add(student);
        }
      }

    } catch (IOException e) {
      e.printStackTrace();

    } catch (Exception e) {
      e.printStackTrace();
    }
    return students;
  }


  @Override
  public boolean update(Object object) {
    try {
      DbConnector dbConnector = DbConnector.loadFromPropertiesFile();

      try (Connection connection = dbConnector.getConnection()) {

        Student student = (Student) object;

        PreparedStatement preparedStatement = connection.prepareStatement(SQLQueries.UPDATE_STUDENT.getQueryString());
        preparedStatement.setString(1, student.getName());
        preparedStatement.setString(2, student.getPhone());
        preparedStatement.setInt(3, student.getId());

        int rowsUpdated = preparedStatement.executeUpdate();

        return rowsUpdated > 0;
      }
    } catch (IOException e) {
      e.printStackTrace();

    } catch (Exception e) {
      e.printStackTrace();
    }
    return false;
  }


  @Override
  public boolean delete(int id) {
    try {
      DbConnector dbConnector = DbConnector.loadFromPropertiesFile();

      try (Connection connection = dbConnector.getConnection()) {

        PreparedStatement preparedStatement = connection.prepareStatement(SQLQueries.DELETE_STUDENT.getQueryString());
        preparedStatement.setInt(1, id);

        int rowsDeleted = preparedStatement.executeUpdate();

        return rowsDeleted > 0;
      }
    } catch (IOException e) {
      e.printStackTrace();

    } catch (Exception e) {
      e.printStackTrace();
    }
    return false;
  }

}
